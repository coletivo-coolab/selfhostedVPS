#!/bin/bash

QUADRO_DIR=$(grep MEDIA_DIR .env | cut -d '=' -f 2-)
mkdir $QUADRO_DIR
chown -R 1000:1000 $QUADRO_DIR
git clone https://github.com/lovasoa/whitebophir.git
