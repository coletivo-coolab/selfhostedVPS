#!/bin/bash

DIR=$(grep DIR .env | cut -d '=' -f 2-)
mkdir $DIR
chown -R 1000:1000 $DIR
